/* eslint-disable node/no-unpublished-import */
/* eslint-disable filenames/match-exported */
import { EOL } from 'node:os';
import type { RollupOptions } from 'rollup';
import packageJSON from './package.json';
import { createSharedConfigs, ensureEnvironmentVariablesSet } from './rollup.config';


function provideReplacements(): Record<string, string> {
  ensureEnvironmentVariablesSet([
    'EMBED_GENERATE_GPG_KEY_FILE_PATH',
    'EMBED_GENERATE_GPG_PASSPHRASE',
    'EMBED_GENERATE_GIT_USER_NAME',
    'EMBED_GENERATE_GIT_GPG_KEY_EMAIL',
  ]);

  return {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_PACKAGE_NAME: JSON.stringify(
      `${packageJSON.name}-generate-dot-files`,
    ),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_PACKAGE_VERSION: JSON.stringify(packageJSON.version),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_PACKAGE_DESCRIPTION: JSON.stringify(
      [
        'Generates temporary changelog for the current execution context merge request',
        'by saving the merge request description in a file inside .changelogs directory.',
      ].join(EOL),
    ),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_GPG_KEY_FILE_PATH: JSON.stringify(
      process.env['EMBED_GENERATE_GPG_KEY_FILE_PATH'],
    ),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_GPG_PASSPHRASE: JSON.stringify(process.env['EMBED_GENERATE_GPG_PASSPHRASE']),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_GIT_USER_NAME: JSON.stringify(process.env['EMBED_GENERATE_GIT_USER_NAME']),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_GIT_GPG_KEY_EMAIL: JSON.stringify(
      process.env['EMBED_GENERATE_GIT_GPG_KEY_EMAIL'],
    ),
  };
}


const rollupConfig: RollupOptions = {
  input: './src/generate-dot-changelog/main',
  output: {
    file: `./bin/${packageJSON.name}-generate-dot-changelog`,
    format: 'cjs',
  },
  ...createSharedConfigs(
    provideReplacements,
  ),
};

export default rollupConfig;
