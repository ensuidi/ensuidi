# Laguna

Generates Changelog From Merge Request Description.

> For **version 3** documentation, checkout [branch **v3**](https://gitlab.com/the-laguna/laguna/-/tree/v3).
>
> For **version 2** documentation, checkout [branch **v2**](https://gitlab.com/the-laguna/laguna/-/tree/v2).
>
> For **version 1** documentation, checkout [branch **v1**](https://gitlab.com/the-laguna/laguna/-/tree/v1).

## Setup

### Self-Managed GitLab

The following instructions assumes your mirror of Laguna project on your self-hosted GitLab instance is located at `https://gitlab.example.com/some-group/laguna`, and the container image registry is available at: `https://registry.gitlab.example.com`.

#### Development Projects

In order to setup Laguna for development projects, i.e., projects that the actual developments are done, you need to follow steps below for every project:

1. Include the provided configurations job in project's `.gitlab-ci.yml` configuration

   ```yml
   stages:
     # ...
     # other stages...
     # ...
     - laguna

   Generate Changelog:
     stage: laguna
     allow_failure: true
     needs: []
     rules:
       - if: $CI_MERGE_REQUEST_IID
         when: manual
     image:
       name: registry.gitlab.example.com/some-group/laguna/generate:v4
       entrypoint:
         - ""
     variables:
       LAGUNA_GENERATE_TOKEN: $LAGUNA_GENERATE_TOKEN
     retry: 1
     interruptible: false
     dependencies: []
     cache: {}
     script:
       - laguna-generate-dot-changelog

   Merge Changelogs:
     stage: laguna
     allow_failure: true
     needs: []
     rules:
       - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
         when: on_success
         changes:
           - .changelogs/*.md
     image:
       name: registry.gitlab.example.com/some-group/laguna/merge:v4
       entrypoint:
         - ""
     variables:
       LAGUNA_MERGE_TOKEN: $LAGUNA_MERGE_TOKEN
     retry: 1
     interruptible: false
     dependencies: []
     cache: {}
     script:
       - laguna-merge-dot-changelogs
   ```

   **Note**: Do not forget to add the `laguna` stage in the `stages` section. Otherwise you will get `invalid ci configuration` error from GitLab.

2. Invite Laguna Bots to project

   - **Laguna Changelog Bot** with `Developer` role

     Go to _Project Information_ > _Members_ under _Invite member_ tab search for **Laguna Changelog Bot** account by his username (`laguna.changelog.bot`) and set his role to `Developer`. The following is an example of a filled form:

     ![CI/CD Variable Setup](https://gitlab.com/the-laguna/laguna/uploads/fbf1087d5f744cedeb90ccff0171f5d6/Pyo65XSrf40QJG85Z9PrvcfSS406Owcql7CLwfPspNCJdnaL-1pnbwfrcfOEtJQF5P5VlEE4uCuvUM1QCMaZ0sIMpLioZrFTEsjF1BZeypRz4-1A0x6dcQfqAOS5kxZ5.png)

   - **Laguna Merge Bot** with `Maintainer` role.

     Go to _Project Information_ > _Members_ under _Invite member_ tab search for **Laguna Merge Bot** account by his username (`laguna.merge.bot`) and set his role to `Maintainer`. The following is an example of a filled form:

     ![CI/CD Variable Setup](https://gitlab.com/the-laguna/laguna/uploads/3e3827b3b41d03a98f1a69363c76db72/vAk8hdafs9YzW6jXkzoYN0k8bJtSVN-XLsVN0mu_tTTaWtgN_dT_4rCLh9DmY2iejm0GUiMoNY6bydMu1WRUDnjyzRIlTM3KUkOwRLf7LrhTTivYQUm4BvdnQWmm37S0.png)

3. Set Laguna Bots tokens

   - Set `LAGUNA_GENERATE_TOKEN` environment variable in your project CI settings.

     It is recommended to [mask](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) this variable, but remember NOT to [protect](https://docs.gitlab.com/ee/ci/variables/#protect-a-cicd-variable) it, because it should be provided in merge request pipelines.

     Go to _Settings_ > _CI/CD_. Open up the _Variables_ section. Click _Add Variable_ button. Fill in the form like the example image below:

     ![CI/CD Variable Setup](https://gitlab.com/the-laguna/laguna/uploads/c0561cc593cc2ca6893a8d1fd97e027e/atYTg35uLRsDIB5BZTxVapswA_B-in673i1zJm2fYQ6cCGLNDglzG4bTwY73UgW70FMhX270CheVu0urOeottrHzoacWfj2b4lROfwRjVwdgER6mC1kFNoz57YnTbE-S.png)

   - Set `LAGUNA_MERGE_TOKEN` environment variable in your project CI settings.

     It is recommended to [mask](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable), and [protect](https://docs.gitlab.com/ee/ci/variables/#protect-a-cicd-variable) this variable. Compared to `LAGUNA_GENERATE_TOKEN`, it is not necessary for this variable to be provided in merge request pipelines.

     Go to _Settings_ > _CI/CD_. Open up the _Variables_ section. Click _Add Variable_ button. Fill in the form like the example image below:

     ![CI/CD Variable Setup](https://gitlab.com/the-laguna/laguna/uploads/d1d7d5663dff5fedf2697461d2fc4b2e/72-3jnqbs7BBQfod0YiEi548W8AMu43UZ8Np4S09mLW7iPBhWtnVgLzMPlEuKYu3uw9G7jeYk55J4i6IV7AA4RJzcyzyxT7XoWCf10jPYrWKjw1YGOHm7Le63vCFozPp.png)

#### Changelog Collection Project

In order to setup Laguna for changelog collection project, i.e., project that all the changelogs are collected in it, you need to follow the steps below:

1. Include the provided configuration job in your `.gitlab-ci.yml` configuration

   ```yml
   stages:
     # other stages...
     - laguna

   Collect Changelogs:
     stage: laguna
     allow_failure: false
     needs: []
     rules:
       - if: $CI_PIPELINE_SOURCE == "schedule" && $LAGUNA_COLLECT_CHANGELOGS == "true"
         when: always
     image:
       name: registry.gitlab.example.com/some-group/laguna/collect:v4
       entrypoint:
         - ""
     variables:
       LAGUNA_COLLECT_TOKEN: $LAGUNA_COLLECT_TOKEN
     retry: 2
     interruptible: false
     dependencies: []
     cache: {}
     script:
       - laguna-collect-changelogs
   ```

   **Note**: Do not forget to add the `laguna` stage in the `stages` section. Otherwise you will get `invalid ci configuration` error from GitLab.

2. Invite Laguna Bot projects

   - Invite **Laguna Collect Bot** with `Maintainer` role to **changelog collection project**.

   Go to _Project Information_ > _Members_ under _Invite member_ tab search for **Laguna Collect Bot** account by his username (`laguna.collect.bot`) and set his role to `Maintainer`. The following is an example of a filled form:

   ![CI/CD Variable Setup](https://gitlab.com/the-laguna/laguna/uploads/f9cbe41750efcaa7d49a0fdd1ba3b497/b3P7h9g7u55UchYKE5_lTjM-WsP6FmSpGqnBhztXVeJVHqcWr7onz87lWH92ZsTLHuj309aFLIYWjtBNGwX8yUdk1qqQNkgrUQ5ZBFNeBXq3JWv5Xbqi3xoChnAVZxxm.png)

   - Invite **Laguna Collect Bot** with `Guest` role to **projects you want to collect changelogs from**.

     Follow the procedure explained above for all the projects yo want the bot to collect their changelogs, except that the role must be set to `Guest`.

3. Set `LAGUNA_COLLECT_TOKEN` environment variable in your project CI settings.

   It is recommended to [mask](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable), and [protect](https://docs.gitlab.com/ee/ci/variables/#protect-a-cicd-variable) this variable.

   Go to _Settings_ > _CI/CD_. Open up the _Variables_ section. Click _Add Variable_ button. Fill in the form like the example image below:

   ![CI/CD Variable Setup](https://gitlab.com/the-laguna/laguna/uploads/17d8e3092d526a9a563377f80a5352fc/pQFTvBXaOUFtfRiEmLMqI-1hcQifbVOf7x8Sh2EZxoFusPxazYcPehSyFDwBcakpEF0xv8W4kk_u4jK2VicAz5kXx1gIJhRPZ2TTHu016DbEzm2DlrJBTUZz1UJl-MOQ.png)

4. Specify projects you want to collect changelogs from

   Add `EXTERNAL_PROJECT_IDS` CI/CD variable in your project CI/CD settings Variables section. This variable is a _Variable_ type variable with **hyphen-separated** list of **numerical project IDs** that you want to collect changelogs from.

   Go to _Settings_ > _CI/CD_. Open up the _Variables_ section. Click _Add Variable_ button. Fill in the form like the example image below (with 4 projects):

   ![CI/CD Variable Setup](https://gitlab.com/the-laguna/laguna/uploads/8eae73335a1ae6d8716752a634a56bb2/PHDSc_h3coBV7zmLD0Q3fP8VxhceeJ3UvT4tGP_bymLTI5qSE-qxwYm2NOcm7kgZxMotJ2EKSsD5maDgsI_JGdTJhnVeuEVwDUPQ1wz--P0C8EAj4RIK74snLlEM9TND.png)

### GitLab.com

TODO

## Generate And Merge Changelogs

1. Run the `Generate Changelog` manual job in merge request page.

   Wait for the job to complete. It should not take more than 10 seconds to complete. If the job failed due to fixable errors, simultaneous `git push`es from the bot and a developer for example, simply rerun the job by clicking the run button from GitLab an mentioned before.

   **Note**: Whenever you changed the merge request description, you will need to rerun the job manually to update the changelog.

   In merge request page you can find the manual button that when it is clicked, the `Generate Changelog` will be started. Below is an example image on a merge request:

   ![CI/CD Job Manual RUn](https://gitlab.com/the-laguna/laguna/uploads/33faa71cff40a0fc899ec3d51155a34d/uPP3G9LKtuwK5JQWSIdVv70CVXqc__2S7UHeCvoHbPY5LIXXftVFGrq4YP6bqMYb1cCUvW3HVNyEGyWdMxLpWKDpDxtdjCXTj9Cy209cOacMWdYhrHbvWuIIFYrrbCL5.png)

   _"It is not a bug, it is a feature!"_

   **Note**: Rerunning the job when there is no change in the description, will not create a new commit, and the job will be succeeded without any changes.

2. Upon merging a merge request, **Laguna Merge Bot** will kick off the `Merge Changelogs` job and he will merge the merge request changelog(s) to the target branch.

   You can simply retry it until it succeeds. If the issue persists, please raise and issue with detailed context information and reproducible environment.

## Collect Changelogs

To collect changelogs from specified projects in schedules, you have to configure a schedule pipeline in the changelog collection project:

Configure a schedule pipeline with `LAGUNA_COLLECT_CHANGELOGS` variable value set to `true`.

Go to _CI/CD_ > _Schedules_. Click _New Schedule_ button. Fill the form and click the _Save pipeline schedule_ button once you are done. The example below is a weekly schedule, you can select other intervals if you'd want to:

![CI/CD Variable Setup](https://gitlab.com/the-laguna/laguna/uploads/89294940e2187526bd89847d516397e9/XkFlTidZnGAX3LfKkKmbdGqT9bNHGAWZ2Rh35tqcwFDnFYz8MWSytaOCnTdszYgvcKh_3LQSu6y2hVJGRgpG6K1OKz_TZUB0GJekH7SLPaRdQmkE77N2xyxBMc7Bzs_w.png)

_Follow the instructions on official documentation to [configure a schedule pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.tml#configuring-pipeline-schedules) and [setting schedule pipeline variables](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#using-variables)._

`Collect Changelog` job will **only** run on this schedule pipeline.

You can simply retry the job in case of uncontrolled job failures, e.g., network issues. If the issue persists, please raise and issue with detailed context information and reproducible environment.

## Configuration

- Date Range Start

  By default, Collect Bot only collects changelogs that are created since his last run from projects. For example, when he collected 4 days ago, and he is going to start now, he will collect from projects only changelogs that are persisted since 4 days ago, and assumes previously persisted changelogs has already been collected by his previous run, which was 4 days ago in this example.

  You can configure the date range start time of collect job in cases you need to force the bot to collect all the changelogs from projects since a specific date. In the previous example, with this option, you can tell him to collect all the changelogs persisted since an specific date, regardless of any previous runs.

  This option works with setting the `LAGUNA_COLLECT_DATE_RANGE_START` CI/CD variable in schedule pipeline of your _changelog collection repository_.

  Currently its value can only be one of the followings:

  - `week` tells the bot to collect changelogs since one week before the start time of his next runs.

  - `month` tells the bot to collect changelogs since one month before the start time of his next runs.

  Of course it is possible to support other values for this option in the future. But, these are the only ones supported for now.

  **Note**: Please make sure not to add any extra following and leading new lines or white-spaces. Otherwise the job will fail with an error.

  To disable this configuration and revert back to the default behavior of collecting changelogs in every run since previous run, _remove_ this configuration variable from schedule pipeline.

## Security Considerations

- Using A Shared Bot

  As this the current solution relies on inviting two members ([Laguna Changelog Bot](https://gitlab.com/laguna.change.bot) and [Laguna Merge Bot](https://gitlab.com/laguna.merge.bot)) to your project(s), or group(s), and using their tokens (`LAGUNA_GENERATE_TOKEN`, and `LAGUNA_MERGE_TOKEN`) in your CI configuration, which one of them, the `LAGUNA_GENERATE_TOKEN` token, is necessary to be provided in Merge Request pipelines, this token is accessible by anyone who can run the job in a Merge Request. With simple change in the `.gitlab-ci.yml` by anyone who has access to, with at least `developer` role, this token can be leaked and used by them in malicious ways. Also, because the token is assigned to bot, they can use the token to access the repositories and other projects they themselves do not have explicit access to them, but the bot does. In other word, they can use the bot token to gain access to other repositories the has access to. Also, as the token currently has the `read_api`, and `write_repository` permissions, they can _read_ all the information about those repositories through API, and _write_ and change source code of those repositories using git, which are required accesses for both bots to operate.

  There are some solutions to this issue:

  - GitLab Future Updates

    There is [an on-going epic on GitLab development](https://gitlab.com/groups/gitlab-org/-/epics/3559), which allows use of [`CI_JOB_TOKEN` predefined environment variable](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html) to change the source code of the repositories the job runs in. Also, is will allow customizing permissions of this token, as they said. As this variable is defined by GitLab during the jobs and are specific to only the job it is provided in, it is safe to be used for this problem too. So, we can wait for the epic to get done and merge into GitLab, then I will update this script accordingly.

  - Building Custom Image And Bot

    _TODO_

  - Specify A Custom CI/CD Configuration File

    This is the easiest and, at the same time, safe solution to improve security of the jobs and the bot token. Follow the instructions in the [official documentation](https://docs.gitlab.com/ee/ci/pipelines/settings.html#specify-a-custom-cicd-configuration-file) for more details. The basic idea is to move the `.gitlab-ci.yml` file outside the project and load it whenever a job runs. This way, even if a malicious code injected to the `.gitlab-ci.yml` file by someone on a arbitrary git branch, it will be overridden by GitLab with the one set in the project settings.
