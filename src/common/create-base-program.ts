import { Command } from 'commander';


export default function createBaseProgram(): Command {
  const program = new Command(EMBED_PACKAGE_NAME);

  program
    .version(EMBED_PACKAGE_VERSION)
    .addHelpCommand(true)
    .addHelpText('beforeAll', `${EMBED_PACKAGE_DESCRIPTION}\n`);

  return program;
}
