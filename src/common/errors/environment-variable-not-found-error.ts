export default class EnvironmentVariableNotFoundError extends Error {
  public constructor(
    public readonly variableName: string,
  ) {
    super();
  }
}
