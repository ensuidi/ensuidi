export default class NotFoundError {
  public constructor(
    public readonly message: string,
  ) {}
}
