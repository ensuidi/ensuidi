export default function buildDotChangelogFilename(
  mergeRequestIid: string,
): string {
  return `merge-request-${mergeRequestIid}.md`;
}
