import { URL } from 'node:url';


interface AddAuthToGitHttpUrlParameters {
  readonly repositoryHttpUrl: string;
  readonly gitUsername: string;
  readonly authToken: string;
}

export default function addAuthToGitHttpUrl(
  parameters: AddAuthToGitHttpUrlParameters,
): string {
  const url = new URL(parameters.repositoryHttpUrl);
  url.username = parameters.gitUsername;
  url.password = parameters.authToken;
  return url.toString();
}
