import assert, { AssertionError } from 'node:assert/strict';
import fs from 'node:fs';
import { posix as path } from 'node:path';
import { format } from 'node:util';
import FailFastError from '@common/errors/fail-fast-error';


export function ensureIsRegularFile(key: string, filepath: string): void {
  try {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const stat = fs.statSync(filepath, { throwIfNoEntry: false });
    assert.strictEqual(stat?.isFile(), true, 'isFile');
    assert.strictEqual(stat.isDirectory(), false, 'isDirectory');
    assert.strictEqual(stat.isBlockDevice(), false, 'isBlockDevice');
    assert.strictEqual(stat.isCharacterDevice(), false, 'isCharacterDevice');
    assert.strictEqual(stat.isFIFO(), false, 'isFIFO');
    assert.strictEqual(stat.isSocket(), false, 'isSocket');
    assert.strictEqual(stat.isSymbolicLink(), false, 'isSymbolicLink');
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      switch (error.message) {
        case 'isFile': {
          throw new FailFastError(`${key} path must point a regular file.`);
        }

        case 'isDirectory': {
          throw new FailFastError(`${key} path must point a regular file, not a directory.`);
        }

        case 'isBlockDevice': {
          throw new FailFastError(`${key} path must point a regular file, not a block device.`);
        }

        case 'isCharacterDevice': {
          throw new FailFastError(`${key} path must point a regular file, not a character device.`);
        }

        case 'isFIFO': {
          throw new FailFastError(`${key} path must point a regular file, not a fifo device.`);
        }

        case 'isSocket': {
          throw new FailFastError(`${key} path must point a regular file, not a socket.`);
        }

        case 'isSymbolicLink': {
          throw new FailFastError(`${key} path must point a regular file, not a symbolic link.`);
        }

        default: {
          throw new FailFastError(`${key} path must point a regular file`);
        }
      }
    }

    throw new FailFastError(
      format(`unknown error occurred while checking ${key} path pointing a normal file: %s`, error),
    );
  }
}

export function ensurePathExists(key: string, filepath: string): void {
  try {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    assert.strictEqual(fs.existsSync(filepath), true);
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError(`${key} file must exists.`);
    }

    throw new FailFastError(
      format(`unknown error occurred while checking for ${key} file existence: %s`, error),
    );
  }
}

export function ensureIsAbsolutePath(key: string, value: string): void {
  try {
    assert.strictEqual(path.isAbsolute(value), true);
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError(`"${key}" is not an absolute path. It is probably an error caused by your customizations, not GitLab. Make sure you did not override this variable accidentally.`);
    }

    throw new FailFastError(
      format(`unknown error occurred while checking for "${key}" environment variable path absoluteness: %s`, error),
    );
  }
}

export function ensureToBeNumberParsable(key: string, value: string): void {
  try {
    const asNumber = Number(value);
    assert.strictEqual(Number.isNaN(asNumber), false);
    assert.strictEqual(Number.isFinite(asNumber), true);
    assert.strictEqual(Number.isSafeInteger(asNumber), true);
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError(`Invalid value set for "${key}" environment variable. Expected a number, found: "${value}"`);
    }

    throw new FailFastError(
      format(`unknown error occurred while validating "${key}" environment variable being a number: %s`, error),
    );
  }
}

export function ensureIsNotEmpty(key: string, value: string): void {
  try {
    assert.strictEqual(value.length > 0, true);
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError(`"${key}" is set, but its value is empty. It is probably an error caused by your customizations, not GitLab. Make sure you did not override this variable accidentally.`);
    }

    throw new FailFastError(
      format(`unknown error occurred while checking for "${key}" environment variable emptiness: %s`, error),
    );
  }
}

export function ensureExists(key: string): void {
  try {
    assert.strictEqual(
      Object.keys(process.env).includes(key),
      true,
    );
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError(
        `"${key}" environment variable is not set. It is probably an error caused by your customizations, not GitLab. Make sure you did not override this variable accidentally.`,
      );
    }

    throw new FailFastError(
      format(`unknown error occurred while checking for "${key}" environment variable existence: %s`, error),
    );
  }
}

export function ensureValidUrl(key: string, value: string): void {
  try {
    (function () {
      return new URL(value);
    })();
  } catch (error: unknown) {
    if (error instanceof TypeError) {
      throw new FailFastError(`"${key}" is not valid url. It is probably an error caused by your customizations, not GitLab. Make sure you did not override this variable accidentally.`);
    }

    throw new FailFastError(
      format(`unknown error occurred while validating "${key}" environment variable for being a valid url: %s`, error),
    );
  }
}

export function ensureIsString(key: string, value: string): void {
  try {
    assert.strictEqual(typeof value, 'string');
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError(`Invalid value set for "${key}" environment variable. Expected a path of type "string", found type: "${error.actual as string}"`);
    }

    throw new FailFastError(
      format(`unknown error occurred while validating "${key}" environment variable type: %s`, error),
    );
  }
}
