import {
  ensureIsAbsolutePath,
  ensureIsNotEmpty,
  ensureIsRegularFile,
  ensurePathExists,
} from './environment-variables';


export default function ensureGPGKeyFile(): void {
  const gpgKeyFilePath = EMBED_GPG_KEY_FILE_PATH;
  const gpgKeyPassphrase = EMBED_GPG_PASSPHRASE;

  ensureIsNotEmpty('GPG_KEY_FILE_PATH', gpgKeyFilePath);
  ensureIsAbsolutePath('GPG_KEY_FILE_PATH', gpgKeyFilePath);
  ensurePathExists('GPG_KEY_FILE_PATH', gpgKeyFilePath);
  ensureIsRegularFile('GPG_KEY_FILE_PATH', gpgKeyFilePath);
  ensureIsNotEmpty('GPG_PASSPHRASE', gpgKeyPassphrase);
}
