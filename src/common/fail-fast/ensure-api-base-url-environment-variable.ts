import assert, { AssertionError } from 'node:assert/strict';
import { format } from 'node:util';
import FailFastError from '@common/errors/fail-fast-error';
import { ensureExists, ensureIsString, ensureValidUrl } from './environment-variables';


function ensureDoesNotHaveUsername(url: URL): void {
  try {
    assert.strictEqual(url.username, '');
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError('"CI_API_V4_URL" contains "username" which is not an expected url. It is probably an error caused by your customizations, not GitLab. Make sure you did not override this variable accidentally.');
    }

    throw new FailFastError(
      format('unknown error occurred while validating "CI_API_V4_URL" environment variable for being a valid url: %s', error),
    );
  }
}

function ensureDoesNotHavePassword(url: URL): void {
  try {
    assert.strictEqual(url.password, '');
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError('"CI_API_V4_URL" contains "password" which is not an expected url. It is probably an error caused by your customizations, not GitLab. Make sure you did not override this variable accidentally.');
    }

    throw new FailFastError(
      format('unknown error occurred while validating "CI_API_V4_URL" environment variable for being a valid url: %s', error),
    );
  }
}

function ensureToBeGitLabApiUrl(url: URL): void {
  try {
    assert.strictEqual(url.pathname, '/api/v4');
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError('"CI_API_V4_URL" is not v4 of GitLab API. It is probably an error caused by your customizations, not GitLab. Make sure you did not override this variable accidentally.');
    }

    throw new FailFastError(
      format('unknown error occurred while validating "CI_API_V4_URL" environment variable for being a valid v4 GitLab API url: %s', error),
    );
  }
}

export default function ensureApiBaseUrlEnvironmentVariable(): void {
  ensureExists('CI_API_V4_URL');

  const value = process.env['CI_API_V4_URL'] ?? '';

  ensureIsString('CI_API_V4_URL', value);
  ensureValidUrl('CI_API_V4_URL', value);

  const url = new URL(value);
  ensureDoesNotHaveUsername(url);
  ensureDoesNotHavePassword(url);
  ensureToBeGitLabApiUrl(url);
}
