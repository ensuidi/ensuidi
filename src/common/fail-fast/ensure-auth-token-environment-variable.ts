import { ensureExists, ensureIsNotEmpty, ensureIsString } from './environment-variables';


export default function ensureAuthTokenEnvironmentVariable(key: string): void {
  ensureExists(key);

  // eslint-disable-next-line security/detect-object-injection
  const value = process.env[key] ?? '';

  ensureIsNotEmpty(key, value);
  ensureIsString(key, value);
}
