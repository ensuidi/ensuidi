export const ONE_SECOND_IN_MILLISECONDS = 1000;
export const UNKNOWN_ERROR_EXIT_CODE = 2;
export const KNOWN_ERROR_EXIT_CODE = 1;
export const COLLECTED_CHANGELOGS_DESTINATION_DIRECTORY_NAME = 'changelog';
