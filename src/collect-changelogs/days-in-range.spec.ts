// eslint-disable-next-line import/no-extraneous-dependencies
import test from 'ava';
import daysInRange from './days-in-range';


test('it should contain the last day when difference is exactly a full day', t => {
  const startDate = new Date('2018-04-04T16:00:00.000Z');
  const endDate = new Date('2018-04-08T16:00:00.000Z');

  t.deepEqual(
    daysInRange(startDate, endDate),
    [
      new Date('2018-04-04T16:00:00.000Z'),
      new Date('2018-04-05T16:00:00.000Z'),
      new Date('2018-04-06T16:00:00.000Z'),
      new Date('2018-04-07T16:00:00.000Z'),
      new Date('2018-04-08T16:00:00.000Z'),
    ],
  );
});

test('it should contain the last day when difference is less than a full day', t => {
  const startDate = new Date('2018-04-04T16:00:00.000Z');
  const endDate = new Date('2018-04-08T10:00:00.000Z');

  t.deepEqual(
    daysInRange(startDate, endDate),
    [
      new Date('2018-04-04T16:00:00.000Z'),
      new Date('2018-04-05T16:00:00.000Z'),
      new Date('2018-04-06T16:00:00.000Z'),
      new Date('2018-04-07T16:00:00.000Z'),
      new Date('2018-04-08T10:00:00.000Z'),
    ],
  );
});

test('it should contain the last day when difference is more than a full day', t => {
  const startDate = new Date('2018-04-04T16:00:00.000Z');
  const endDate = new Date('2018-04-08T23:00:00.000Z');

  t.deepEqual(
    daysInRange(startDate, endDate),
    [
      new Date('2018-04-04T16:00:00.000Z'),
      new Date('2018-04-05T16:00:00.000Z'),
      new Date('2018-04-06T16:00:00.000Z'),
      new Date('2018-04-07T16:00:00.000Z'),
      new Date('2018-04-08T23:00:00.000Z'),
    ],
  );
});
