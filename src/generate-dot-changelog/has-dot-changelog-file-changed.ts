import { createDefaultExec } from '@common/exec';


function hasFileChanged(gitStatusOutput: string): boolean {
  return gitStatusOutput.trim().length > 0;
}

export default function hasDotChangelogFileChanged(
  filepath: string,
): boolean {
  const exec = createDefaultExec();

  const statusOutput = exec(
    'git',
    ['status', '--short', '--porcelain=1', filepath],
    [],
  ).stdout;


  return hasFileChanged(statusOutput);
}
