import buildDotChangelogFilename from '@common/build-dot-changelog-filename';
import { KNOWN_ERROR_EXIT_CODE, UNKNOWN_ERROR_EXIT_CODE } from '@common/constants';
import createBaseProgram from '@common/create-base-program';
import createDotChangelogsDirectory from '@common/create-dot-changelogs-directory';
import HandledError from '@common/errors/handled-error';
import { createDefaultExec } from '@common/exec';
import prepareGPGAgent from '@common/gpg/prepare-gpg-agent';
import resolveDotChangelogFilePath from '@common/resolve-dot-changelog-file-path';
import failFast from './fail-fast';
import getMergeRequestDescription from './gitlab/get-merge-request-description';
import hasDotChangelogFileChanged from './has-dot-changelog-file-changed';
import writeDotChangelog from './write-dot-changelog';


async function run(): Promise<void> {
  try {
    failFast();

    if (
      undefined === process.env['CI_MERGE_REQUEST_IID']
      || undefined === process.env['CI_COMMIT_REF_NAME']
      || undefined === process.env['CI_REPOSITORY_URL']
      || undefined === process.env['CI_API_V4_URL']
      || undefined === process.env['LAGUNA_GENERATE_TOKEN']
      || undefined === process.env['CI_PROJECT_ID']
    ) {
      console.error('required environment variable is not defined, even though we check it before. weird! exiting.');
      process.exitCode = KNOWN_ERROR_EXIT_CODE;
      return;
    }

    prepareGPGAgent(
      process.env['CI_COMMIT_REF_NAME'],
      process.env['CI_REPOSITORY_URL'],
      process.env['LAGUNA_GENERATE_TOKEN'],
    );

    const dotChangelogsDirectory = createDotChangelogsDirectory();
    const dotChangelogFilename = buildDotChangelogFilename(process.env['CI_MERGE_REQUEST_IID']);
    const dotChangelogFilePath = resolveDotChangelogFilePath(
      dotChangelogsDirectory,
      dotChangelogFilename,
    );

    const description = await getMergeRequestDescription({
      apiBaseUrl: process.env['CI_API_V4_URL'],
      authToken: process.env['LAGUNA_GENERATE_TOKEN'],
      mergeRequestIid: Number.parseInt(process.env['CI_MERGE_REQUEST_IID'], 10),
      projectId: Number.parseInt(process.env['CI_PROJECT_ID'], 10),
    });

    writeDotChangelog(dotChangelogFilePath, description);
    const exec = createDefaultExec();

    if (!hasDotChangelogFileChanged(dotChangelogFilePath)) {
      console.debug('no change in temporary changelog file for the merge request has been detected. exiting.');
      process.exitCode = 0;
      return;
    }

    exec('git', ['add', dotChangelogFilePath], []);
    exec(
      'git',
      [
        'commit',
        '-m',
        'docs: add merge request changelog',
        '-m',
        [
          'changelog for merge request',
          `!${process.env['CI_MERGE_REQUEST_IID']}`,
          'generated from its description',
          `by @${EMBED_GIT_USER_NAME}`,
        ].join(' '),
      ],
      [],
    );
    exec('git', ['push'], []);
    const commitId = exec('git', ['rev-parse', 'HEAD'], []).stdout.trim();
    console.debug(commitId);
  } catch (error: unknown) {
    if (error instanceof HandledError) {
      error.print();
      process.exitCode = error.exitCode;
      return;
    }

    console.error('unknown error: %s', error);
    process.exitCode = UNKNOWN_ERROR_EXIT_CODE;
  }
}

function generateDotChangelog(): void {
  const program = createBaseProgram();

  program
    .action(async () => await run());

  program
    .parse(process.argv);
}

generateDotChangelog();
