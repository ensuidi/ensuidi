import fs from 'node:fs';
import { format } from 'node:util';
import { KNOWN_ERROR_EXIT_CODE } from '@common/constants';
import HandledError from '@common/errors/handled-error';


export default function writeDotChangelog(
  dotChangelogFilePath: string,
  content: string,
): void {
  try {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    fs.writeFileSync(dotChangelogFilePath, content, { encoding: 'utf8' });
  } catch (error: unknown) {
    if (error instanceof AggregateError || error instanceof Error) {
      throw new HandledError(
        format('unable to wrote to changelog file: %s', error.message),
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    throw error;
  }
}
