import fs from 'node:fs';
import { KNOWN_ERROR_EXIT_CODE } from '@common/constants';
import { resolveDotChangelogsDirectoryPath } from '@common/create-dot-changelogs-directory';
import HandledError from '@common/errors/handled-error';
import isFilenameAValidDotChangelogFilename from '@common/is-filename-a-valid-dot-changelog-filename';


function directoryExists(directoryPath: string): boolean {
  try {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    return fs.existsSync(directoryPath);
  } catch {
    throw new HandledError(
      'unable to check existence of .changelogs directory',
      KNOWN_ERROR_EXIT_CODE,
    );
  }
}

function containsAnyChangelogFile(directoryPath: string): boolean {
  try {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const files = fs.readdirSync(directoryPath, { encoding: 'utf8', withFileTypes: true });

    return undefined !== files.find(
      filename => filename.isFile()
      && isFilenameAValidDotChangelogFilename(filename.name),
    );
  } catch {
    throw new HandledError(
      'unable to check .changelogs directory files for temporary changelog files',
      KNOWN_ERROR_EXIT_CODE,
    );
  }
}

export default function anyDotChangelogExists(): boolean {
  const dotChangelogsDirectoryPath = resolveDotChangelogsDirectoryPath();

  if (!directoryExists(dotChangelogsDirectoryPath)) {
    return false;
  }

  if (!containsAnyChangelogFile(dotChangelogsDirectoryPath)) {
    return false;
  }

  return true;
}
