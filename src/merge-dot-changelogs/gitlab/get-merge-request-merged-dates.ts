import { format } from 'node:util';
import dayjs from 'dayjs';
import got from 'got';
import type { Response } from 'got';
import { NOT_FOUND, UNAUTHORIZED } from 'http-status';
import mapSeries from 'p-map-series';
import { KNOWN_ERROR_EXIT_CODE } from '@common/constants';
import HandledError from '@common/errors/handled-error';
import InvalidMergeRequestMergedAtDate from '@common/errors/invalid-merge-request-merged-at-date';
import NotFoundError from '@common/errors/not-found-error';
import UnauthorizedError from '@common/errors/unauthorized-error';
import UnknownGitlabError from '@common/errors/unknown-gitlab-error';
import type NotFoundResponseBody from '@common/gitlab/not-found-response-body';


export interface GetMergeRequestMergedDatesContext {
  readonly authToken: string;
  readonly apiBaseUrl: string;
  readonly projectId: string;
}

interface GitLabGetSingleMergeRequestRawResponse {
  readonly id: number;
  readonly iid: number;
  readonly state: 'opened' | 'closed' | 'locked' | 'merged';
  readonly merged_at: string;
  readonly merged_by: {
    readonly id: number;
    readonly name: string;
    readonly username: string;
    readonly state: string;
    readonly avatar_url: string;
    readonly web_url: string;
  };
}

interface GitLabGetSingleMergeRequestResponse {
  readonly iid: number;
  readonly state: 'opened' | 'closed' | 'locked' | 'merged';
  readonly mergedAt: string;
}

export interface MergeRequestWithMergedDate {
  readonly iid: string;
  readonly mergedDate: Date;
}

function validateMergedAtDate(
  mergeRequestIid: string,
  mergeAtDateString: string,
): void {
  if (!dayjs(mergeAtDateString).isValid()) {
    throw new InvalidMergeRequestMergedAtDate(
      mergeAtDateString,
      mergeRequestIid,
    );
  }
}

function parseResponseMergedDate(dateString: string): Date {
  return dayjs(dateString).toDate();
}

function responseToMergedDate(
  response: GitLabGetSingleMergeRequestResponse,
): Date {
  return parseResponseMergedDate(response.mergedAt);
}

function parseRawResponse(
  raw: GitLabGetSingleMergeRequestRawResponse,
): GitLabGetSingleMergeRequestResponse {
  return {
    iid: raw.iid,
    mergedAt: raw.merged_at,
    state: raw.state,
  };
}

async function requestMergeRequestMergedDate(
  context: GetMergeRequestMergedDatesContext,
  mergeRequestIid: string,
): Promise<MergeRequestWithMergedDate> {
  try {
    const url = `${context.apiBaseUrl}/projects/${context.projectId}/merge_requests/${mergeRequestIid}`;
    const response = await got
      .get(url, {
        headers: {
          Authorization: `Bearer ${context.authToken}`,
        },
      })
      .json<GitLabGetSingleMergeRequestRawResponse>();

    const parsedResponse = parseRawResponse(response);
    validateMergedAtDate(mergeRequestIid, parsedResponse.mergedAt);

    return {
      iid: parsedResponse.iid.toString(10),
      mergedDate: responseToMergedDate(parsedResponse),
    };
  } catch (error: unknown) {
    if (error instanceof got.HTTPError) {
      if (error.response.statusCode === UNAUTHORIZED) {
        throw new UnauthorizedError();
      }

      if (error.response.statusCode === NOT_FOUND) {
        throw new NotFoundError(
          (error.response as Response<NotFoundResponseBody>)
            .body
            .message
            ?? 'Project or merge request not found',
        );
      }

      throw new UnknownGitlabError(
        error.response.statusMessage
          ?? (error.response.body as string | undefined)
          ?? 'unknown error occurred',
      );
    }

    throw error;
  }
}

async function requestMergeRequestMergedDates(
  context: GetMergeRequestMergedDatesContext,
  mergeRequestIids: readonly string[],
): Promise<MergeRequestWithMergedDate[]> {
  return await mapSeries(
    mergeRequestIids,
    async mergeRequestIid => await requestMergeRequestMergedDate(
      context,
      mergeRequestIid,
    ),
  );
}

export default async function getMergeRequestMergedDates(
  context: GetMergeRequestMergedDatesContext,
  mergeRequestIids: readonly string[],
): Promise<MergeRequestWithMergedDate[]> {
  try {
    return await requestMergeRequestMergedDates(context, mergeRequestIids);
  } catch (error: unknown) {
    if (error instanceof NotFoundError) {
      throw new HandledError(
        'not found error received from GitLab while requesting for merge requests information. double check if the provided access token is not expired and has the "read_api" permission scope. if the problem is not with the access token. there maybe a changelog file that its respective merge request has already been deleted completely or does not exists at all.',
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    if (error instanceof UnauthorizedError) {
      throw new HandledError(
        'unauthorized error received from GitLab while requesting for merge requests information. double check if the provided access token is not expired and has the "read_api" permission scope.',
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    if (error instanceof UnknownGitlabError) {
      throw new HandledError(
        format(
          'unknown error received from GitLab while requesting for merge requests information %s',
          error.message,
        ),
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    if (error instanceof InvalidMergeRequestMergedAtDate) {
      throw new HandledError(
        format(
          'unable to parse merge request (%s) merged date "%s". make sure the merge request is marked as merged in GitLab.',
          error.mergeRequestIid,
          error.mergedAtDate,
        ),
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    throw new HandledError(
      'unknown error received from GitLab while requesting for merge requests information',
      KNOWN_ERROR_EXIT_CODE,
    );
  }
}
